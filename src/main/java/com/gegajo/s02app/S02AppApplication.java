package com.gegajo.s02app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S02AppApplication {

	public static void main(String[] args) {
		SpringApplication.run(S02AppApplication.class, args);
	}

}
