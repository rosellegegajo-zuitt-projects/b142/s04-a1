package com.gegajo.s02app.services;

import com.gegajo.s02app.models.User;
import com.gegajo.s02app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void createUser(User newUser){
        userRepository.save(newUser);
    }

    @Override
    public void updateUser(Long id, User updateUser){
        User existingUser = userRepository.findById(id).get();
        existingUser.setUsername(updateUser.getUsername());
        existingUser.setPassword(updateUser.getPassword());
        userRepository.save(existingUser);
    }

    @Override
    public void deleteUser(Long id){
        userRepository.deleteById(id);
    }

    @Override
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

}
