package com.gegajo.s02app.services;

import com.gegajo.s02app.models.Post;
import com.gegajo.s02app.models.User;
import com.gegajo.s02app.repositories.PostRepository;
import com.gegajo.s02app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImplementation implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;


    public void createPost(Post newPost, Long userId) {
        User author = userRepository.findById(userId).get();
        Post post = new Post();
        post.setTitle(newPost.getTitle());
        post.setContent(newPost.getContent());
        post.setUser(author);
        postRepository.save(post);

    }

    @Override
    public void updatePost(Long userId, Long postId, Post updatedPost) {
        Post existingPost = postRepository.findById(postId).get();
        existingPost.setTitle(updatedPost.getTitle());
        existingPost.setContent(updatedPost.getContent());
        User newAuthor = userRepository.findById(userId).get();
        existingPost.setUser(newAuthor);
        postRepository.save(existingPost);

    }

    @Override
    public void deletePost(Long id) {
        postRepository.deleteById(id);

    }

    @Override
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    @Override
    public Iterable<Post> getMyPosts(Long userId) {
        User author = userRepository.findById(userId).get();
        return author.getPosts();
    }
}
