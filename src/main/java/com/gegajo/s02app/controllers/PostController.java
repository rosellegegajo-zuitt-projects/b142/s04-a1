package com.gegajo.s02app.controllers;

import com.gegajo.s02app.models.Post;
import com.gegajo.s02app.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value="/users/{userId}/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post newPost, @PathVariable Long userId){
        postService.createPost(newPost, userId);
        return new ResponseEntity<>("New Post was created", HttpStatus.CREATED);
    }

    //Retrieve all post
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPots(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    //Retrieve all post
    @RequestMapping(value="/users/{userId}/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPots(@PathVariable Long userId){
        return new ResponseEntity<>(postService.getMyPosts(userId), HttpStatus.OK);
    }

    //Update a posts
    @RequestMapping(value="/users/{userId}/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long userId, @PathVariable Long postId, @RequestBody Post updatedPost){
        postService.updatePost(userId, postId, updatedPost);
        return new ResponseEntity<>("Post was updated.", HttpStatus.OK);
    }

    //Delete
    @RequestMapping(value="/posts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long id){
        postService.deletePost(id);
        return new ResponseEntity<>("Posts was deleted", HttpStatus.OK);
    }
}
